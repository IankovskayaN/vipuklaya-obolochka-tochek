#define _CRT_SECURE_NO_WARNINGS		// �� �������� �������������� ��� ������������� fopen, fscanf

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "Header.h"

using namespace std;

// �������� ����� �� �����. ������� ������� - ��� �����
bool PolygonBuilder::Load(char *fname)
{
	FILE *f;			// ���������� ��������� �� ���������, ����������� ����
	char s[50], *ptr;	// ����� ��� �������� ��������� ������ � ��������� �� ������ (��� ������� strtod)
	double x, y;		// ���������� �����

	n = 0;								// ���-�� �����(���� �� ������� �� �����)
	f = fopen(fname, "rt");				// ������� ���� ��� ������ � ��������� ������
	if (f == NULL)						// ���� ������� �� �������
		return false;					// ����� �� ������� � ������� false (������)

	while (true)							// ���� ���������� �����
	{
		if (fscanf(f, "%50s", s) == EOF)	// ��������� ����� ��� ������
			break;						
		x = strtod(s, &ptr);			// ������������� ��������� ������ � ����� x
		if (*ptr != '\0')				// ���� ���� ������ �� '\0', �� ���� �� ����� ������, ������ � ������ ���� �� �����, �����
		{
			fclose(f);					// ������� ����
			return false;				// ����� � ������� ������
		}

		if (fscanf(f, "%50s", s) == EOF) // �� �� ����� ��� y
			break;
		y = strtod(s, &ptr);
		if (*ptr != '\0')
		{
			fclose(f);
			return false;
		}

		Data[n].x = x;					// ���������� � ������ �������� �����
		Data[n].y = y;					// ��������� ��������
		n++;							

		if (n >= MAX)					// ���������� ����� ����� ������������ ���������
			break;
	}

	fclose(f);							// ������� ����

	return true;						// ����� � ������� true (���� �������� �������)
}

/*
������� Side ���������� ������������� ��������, ���� ����� C ��������� � ������ ������������� ������������ ������� AB
�������������, ���� ����� C ��������� � ����� ������������� ������������ ������� AB,
0,	���� ����� C ��������� �� ����� ������ � �������� AB.
��� �������� �������� ����������� z ���������� ������������ [AC, AB] � ��������� ������������.
*/
double PolygonBuilder::Side(Point A, Point B, Point C)
{
	double ACx, ACy, ABx, ABy;		// ���������� �������� AC � AB

	ACx = C.x - A.x;				// ���������� ��������� ������� AC
	ACy = C.y - A.y;

	ABx = B.x - A.x;				// ���������� ��������� ������� AB
	ABy = B.y - A.y;

	return ACx * ABy - ACy * ABx;	// ���������� ���������� z ���������� ������������
}

// ����� ������ ������� �������� �������� �����
bool PolygonBuilder::FindFirstEdge(void)
{
	int i, j, k;	
	bool right;		// ������� ����, ��� ��� ����� ������

	for (i = 0; i < n; i++)			// ������� ����� ������ �������
		for (j = i + 1; j < n; j++)	// ������� ����� ����� �������
		{
			right = true;
			for (k = 0; k < n; k++)	// ������� ���� ����� 
				right = right && (Side(Data[i], Data[j], Data[k]) >= 0);	// ����� �� ��� ��� ������ �� �������

			if (right)					// ���� ��� ������ (��� �� �������), �� ������� - ��� ������� ��������
			{
				Polygon[0] = Data[i];	// �������� ����� ������ ��������� ������� ��������
				Polygon[1] = Data[j];	// �������� ����� ����� ��������� ������� ��������
				m = 2;					// �������� ���������� ��������� �����
				return true;			
			}
		}

	return false;		// ���� ������� ������� �� �������
}

// ���������� �������� �������� �����
bool PolygonBuilder::Create(void)
{
	Point A, B;	// A - ������ �������, B - �����
	int i, j;	
	bool right;	// ������� ����, ��� ��� ����� ������

	if (n < 2) // ���� ���������� ����� ������ 2-�, �� ������ ��������� �������
		return false;

	//���� ������� ������ ������ ������� ������� false
	if (!FindFirstEdge())
		return false;		//���������� ������ � ����� ������� false.

							// ����� ���������� ������� FindFirstEdge
							// � ������� Polygon ����� ������ 2 ������� ��������
							// ���� ��������� ������� ������� �� ������ �����:
	A = Polygon[1];

	// ���� ���� �����������, ���� �������� �������� ����� �� ���������
	while (!(A.x == Polygon[0].x && A.y == Polygon[0].y))
	{
		for (i = 0; i < n; i++)	// ����� ����� 
		{
			B = Data[i];					
			if (A.x == B.x && A.y == B.y)	// ���� ��� ��������� � ������ A, �� �� ��������
				continue;

			right = true;					// ������� ���� �����
			for (j = 0; j < n; j++)
				right = right && Side(A, B, Data[j]) >= 0;		// ����� �� ��� ��� ������ �� ������� AB ��� �� ������� AB

			if (right)						// ���� ��� ������ (��� �� �������), �� ����� ������� �������� �������� ��������
			{
				Polygon[m++] = Data[i];		// �������� ������� � ������ ������ ��������, ��������� ������� ������,
				A = Data[i];				// �� ������ ��������� ������� ������� ��������� �����
				break;						// � �������� ����� �������
			}
		}
	}

	m--;			// ���������� ��������� �����, ��� ��� ��� ��������� � ������

	return true;	// �������� ���������, ������� true
}

// ���������� ������ �������� �������� � ����
bool PolygonBuilder::Save(char *fname)
{
	FILE *f;				
	int i;					

	f = fopen(fname, "wt");	// ������� ���� ��� ������ � ��������� ������
	if (f == NULL)			// ���� ������� �� �������, ������� false
		return false;

	fprintf(f, "���������� �����: %d\n", n);				// ������� ���������� �������� �����
	fprintf(f, "���������� ������ �������� �������� �����: %d\n\n", m);	// ������� ���������� ������ ����������� ��������

															// ������� ���������� ������
	for (i = 0; i < m; i++)
		fprintf(f, "%.4lf\t%.4lf\n", Polygon[i].x, Polygon[i].y);

	fclose(f);		// ������� ����

	return true;	
}

int main(int argc, char* argv[])

{
	class PolygonBuilder P;	// ���������� ������� ������ PolygonBuilder

	setlocale(LC_ALL, "Russian");

	// ��� ��������� ���� ��������� ���������� ��� ����� ������ � �������� ������,
	// ������� ����� ���������� ���������� ������ ���� ����� 3.
	printf("������� ���� exe, �������� ���� txt � �������, ���� ������ txt \n");
	if (argc != 3)	// ���� �� ����� 3, �� ������ ��������� � ��������� ������ � ����� 1
	{
		printf("������ � ���������� ����������.\n");
		printf("������� obolochka.exe; ��� ����� txt � �������; ��� ����� ������ txt \n");
		system("pause");
		return 1;
	}

	// ��������� ���� � �������
	if (!P.Load(argv[1]))	// ���� ������� �������� ������� false, ������ ���� ��������� �� �������
	{
		printf("������ � ����� ������");
		system("pause");
		return 1;
	}

	// ���������� ��������
	if (!P.Create())			// ���� ������� ���������� ������� false, ������ ���������� �� �������
	{
		printf("���������� ��������� �������� ��������");
		system("pause");
		return 1;
	}

	// C��������� ����������
	if (!P.Save(argv[2]))	// ���� ������� ���������� ������� false, ������ ���� �������� �� �������
	{
		printf("���������� �������� ����");
		system("pause");
		return 1;
	}

	// �������� �� �������� ��������� ������
	printf("�������� �������� ����� ��������� �������.\n");
	printf("��������� ������� � ���� %s\n", argv[2]);
	system("pause");
	return 0;
}
